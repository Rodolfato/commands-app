const router = require('express').Router();
let Command = require('../models/command.model');

router.route('/').get((req, res) => {
    Command.find()
        .then(commands => res.json(commands))
        .catch(err => res.status(400).json('Error: ' + err));

});

router.route('/add').post((req, res) => {
    const name = req.body.name;
    const description = req.body.description;

    const newCommand = new Command({
        name,
        description,
    });

    newCommand.save()
        .then(() => res.status(200).json('Command added!'))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/:id').get((req, res) => {
    const id = req.params.id;

    Command.findById(id)
        .then(command => res.status(200).json(command))
        .catch(err => res.status(400).json('Error: ' + err));
});

router.route('/delete/:id').delete((req, res) => {
    const id = req.params.id;

    Command.findByIdAndDelete(id)
        .then(() => res.status(204).json('Command deleted!'))
        .catch(err => res.status(400).json('Error: ' + err))
});

router.route('/update/:id').put(async (req, res) => {

    try {
        await Command.findByIdAndUpdate(req.params.id, req.body);
        res.status(200).json('Command updated!');
    } catch (err) {
        console.error(err.message);
        res.send(400).send('Server error');
    }

});

module.exports = router;