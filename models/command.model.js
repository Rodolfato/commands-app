const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const commandSchema = new Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    description: {
        type: String,
        required: true,
        trim: true
    },
},
{
        timestamps: true,
});

const Command = mongoose.model('Command', commandSchema);
module.exports = Command;