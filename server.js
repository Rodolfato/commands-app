const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose')
const commandsRouter = require('./routes/commands')

require('dotenv').config();

const app = express();
const port = 5000;
const database_name = process.env.DB_NAME;
const database_username = process.env.DB_USERNAME;
const database_password = process.env.DB_PASSWORD;
const database_port = process.env.DB_PORT;
const database_ip = process.env.DB_IP;

app.use(cors());
app.use(express.json());

//const uri = "mongodb+srv://admin:p455w0rd@cluster0.vk8tu.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const uri = "mongodb://admin:p455w0rd@cluster0-shard-00-00.vk8tu.mongodb.net:27017,cluster0-shard-00-01.vk8tu.mongodb.net:27017,cluster0-shard-00-02.vk8tu.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-si185h-shard-0&authSource=admin&retryWrites=true&w=majority"
const db_uri = `${database_name}://${database_username}:${database_password}@${database_ip}:${database_port}`
//mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true, dbName: 'commands' });

//mongoose.createConnection()


/*const connection = mongoose.connection;
connection.once('open', () => {
    console.log("MongoDB database connection established successfully.");

});*/


mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true, dbName: 'commands' }, (err) => {
    if (err) {
        console.log(`Error al conectarse a la base de datos\nURI=${uri}`);
        console.log(err)
    }
    else {
        console.log('MongoDB database conexion exitosa.')
    }
});


app.use('/commands', commandsRouter);
app.get('/', (req, res) => {
    res.send('<h1> Aplicacion cargada correctamente! </h1>')
});

app.listen(port, () => {
    console.log(`Server is running on port with NEW conn string: ${port}`);
    console.log('This is an update. Will it work?');
});